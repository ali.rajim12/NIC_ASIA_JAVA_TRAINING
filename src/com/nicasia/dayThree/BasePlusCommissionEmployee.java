package com.nicasia.dayThree;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class BasePlusCommissionEmployee extends CommissionEmployee {

    private double baseAmount;

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    @Override
    public double totalEarning() {
        return super.totalEarning() + getBaseAmount();
    }
}
