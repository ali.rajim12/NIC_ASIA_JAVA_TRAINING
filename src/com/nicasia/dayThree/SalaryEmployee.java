package com.nicasia.dayThree;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class SalaryEmployee extends Employee {

    private double monthlySalary;

    @Override
    public double totalEarning() {
        return getMonthlySalary();
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public SalaryEmployee createSalaryEmployee(String fullName,
                                     String employeeId,
                                     double monthlySalary) {
        SalaryEmployee salaryEmployee = new SalaryEmployee();
        salaryEmployee.setFullName(fullName);
        salaryEmployee.setMonthlySalary(monthlySalary);
        salaryEmployee.setEmployeeId(employeeId);
        return salaryEmployee;
    }
}
