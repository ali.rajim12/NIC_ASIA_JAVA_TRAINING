package com.nicasia.dayThree;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class HourlyEmployee extends Employee {

    private double ratePerHour;

    private int totalHourWorked;

    @Override
    public double totalEarning() {
        if(getRatePerHour() > 100) {
            return 100 * getRatePerHour()
                    + (getTotalHourWorked() - 100) * getRatePerHour()*1.5;
        }
        return getRatePerHour()*getTotalHourWorked();
    }

    public double getRatePerHour() {
        return ratePerHour;
    }

    public void setRatePerHour(double ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    public int getTotalHourWorked() {
        return totalHourWorked;
    }

    public void setTotalHourWorked(int totalHourWorked) {
        this.totalHourWorked = totalHourWorked;
    }
}
