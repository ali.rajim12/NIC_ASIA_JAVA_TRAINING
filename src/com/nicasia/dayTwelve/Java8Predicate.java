package com.nicasia.dayTwelve;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-08.
 * @project IntelliJ IDEA
 */
public class Java8Predicate {
    /**
     * Java this class test for predicate example
     *
     * @param args
     */
    public static void main(String[] args) {

        List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Predicate<Integer> greaterThen5 = x -> x > 5;
        Predicate<Integer> lessThenEight = x -> x < 8;

        List<Integer> filteredInteger = integerList.stream()
                .filter(greaterThen5)
                .collect(Collectors.toList());

        filteredInteger.forEach(System.out::println);

        /**
         * Predicate with and condition
         */
        List<Integer> filteredInteger1 = integerList.stream()
                .filter(greaterThen5.and(lessThenEight))
                .collect(Collectors.toList());

        filteredInteger1.forEach(System.out::println);

        /**
         * Multiply each member of function
         */

        Function<Integer, Integer> multipleOf3 = x-> x*3;

        List<Integer> multipleOf3List = filteredInteger1.stream()
                .map(multipleOf3)
                .collect(Collectors.toList());

        multipleOf3List.forEach(System.out::println);
    }
}
