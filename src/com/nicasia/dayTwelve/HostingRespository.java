package com.nicasia.dayTwelve;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-08.
 * @project IntelliJ IDEA
 */
public class HostingRespository {

    public static List<Hosting> filterHosting(List<Hosting> hosting,
                                              Predicate<Hosting> predicate) {
        return hosting.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
