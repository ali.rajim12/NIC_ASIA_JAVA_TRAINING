package com.nicasia.dayTwelve;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-08.
 * @project IntelliJ IDEA
 */
public class Java8Predicate7 {
    public static void main(String[] args) {
        Hosting h1 = new Hosting(1, "amazon", "aws.amazon.com");
        Hosting h2 = new Hosting(2, "linode", "linode.com");
        Hosting h3 = new Hosting(3, "liquidweb", "liquidweb.com");
        Hosting h4 = new Hosting(4, "google", "google.com");

        /**
         * Defining array list of hosting
         */

        List<Hosting> list = Arrays.asList(new Hosting[]{h1, h2, h3, h4});

        /**
         * Defining multiple predicates
         */

        Predicate<Hosting> idGreaterThan2 = hosting -> hosting.getId() > 2;
        Predicate<Hosting> urlStartsWithA = hosting -> hosting.getUrl().startsWith("a");
        Predicate<Hosting> nameWithLenghtGT2 = hosting -> hosting.getName().length() >2;

        /**
         * Using multiple for loops
         */
        List<Hosting> result = list.stream()
                .filter(idGreaterThan2)
                .collect(Collectors.toList());

        List<Hosting> result1 = list.stream()
                .filter(urlStartsWithA)
                .collect(Collectors.toList());

        List<Hosting> result2 = list.stream()
                .filter(nameWithLenghtGT2)
                .collect(Collectors.toList());

        /**
         * Using the method of HostingRepository
         */

        List<Hosting> idHavingGreaterThan2 = HostingRespository.filterHosting(
                list, idGreaterThan2
        );

        List<Hosting> urlStartsWithAList = HostingRespository.filterHosting(
                list, urlStartsWithA
        );

        List<Hosting> nameHavingLengthGt2 = HostingRespository.filterHosting(
                list, nameWithLenghtGT2
        );

        idHavingGreaterThan2.forEach(System.out::println);
        urlStartsWithAList.forEach(System.out::println);
        nameHavingLengthGt2.forEach(System.out::println);





    }
}
