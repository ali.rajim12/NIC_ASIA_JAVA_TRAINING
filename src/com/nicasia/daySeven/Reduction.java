package com.nicasia.daySeven;

import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author rajim 2021-09-03.
 * @project IntelliJ IDEA
 */
public class Reduction {
    public static void main(String[] args) {


        int optionalInt = IntStream
                .range(1, 4)
                .reduce(10, (a, b)->
                        (a+b) );
        System.out.println(optionalInt);
        int sum = IntStream
                .range(1, 4)
                .sum();

        Integer[] spam = new Integer[] {1, 2, 3, 4, 5, 6, 7 };
        List<Integer> list = Arrays.asList(spam);

        IntSummaryStatistics intSummaryStatistics = list.stream()
                .filter(e-> e <4)
                .mapToInt(e -> e)
                .summaryStatistics();
        
        System.out.println(intSummaryStatistics);


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("cat");
        arrayList.add("dog");
        arrayList.add("elephant");
        arrayList.add("fox");
        arrayList.add("rabbit");
        arrayList.add("duck");


        Map<Integer, String>  map = new HashMap<>();
        map.put(1, "cat");
        map.put(2, "dog");
        map.put(3, "elephant");
        map.put(4, "fox");
        map.put(5, "rabbit");
        map.put(6, "duck");

        Stream<String> stringStream = arrayList.stream()
                .takeWhile(n -> n.length() % 2 != 0);

        stringStream.forEach(System.out::println);

        Stream<Map.Entry<Integer, String>> entryStream = map.entrySet()
                .stream()
                .takeWhile(n -> n.getValue().length() % 2 != 0);
        entryStream.forEach(System.out::println);


//        OptionalDouble optionalDouble = DoubleStream.empty().reduce();

    }
}
