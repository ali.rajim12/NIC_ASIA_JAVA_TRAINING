package com.nicasia.daySeven;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-03.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        stringList.add("a");
        stringList.add("b");
        stringList.add("c");
        stringList.add("d");
        stringList.add("e");
        stringList.add("f");
        stringList.add("g");

//        List<String> requiredList = new ArrayList<>();
//        for(String required: stringList) {
//            if(required.equals("a")
//                    || required.equals("b")
//                    || required.equals("c")) {
//                requiredList.add(required);
//            }
//        }

        List<String> stringList1 = stringList
                .stream()
                .filter(e-> (e.equals("a") || e.equals("b") || e.equals("c")))
                .collect(Collectors.toList());

        stringList1.forEach(System.out::println);
//        stringList1.forEach(e-> System.out.println(e.getBytes()));
    }

    private static String collectRequired(String e) {
        if(e.equals("a") || e.equals("b") || e.equals("c")) {
            return e;
        }
        return null;
    }
}
