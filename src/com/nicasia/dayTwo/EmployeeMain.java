package com.nicasia.dayTwo;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-08-25.
 * @project IntelliJ IDEA
 */
public class EmployeeMain {
    public static void main(String[] args) {

//        SalaryEmployee salaryEmployee = new SalaryEmployee(
//                "Test",
//                "1",
//                1000);
        // Create empty list

        List<SalaryEmployee> salaryEmployeeList = new ArrayList<>();

        salaryEmployeeList.add(createNewEmployee("Rajim", "1", 1000));
        salaryEmployeeList.add(createNewEmployee("Rajim", "1", 1000));

        SalaryEmployee salaryEmployee1 = new SalaryEmployee();
        salaryEmployee1.setMonthlySalary(1000);
        salaryEmployee1.setEmployeeId("2");
        salaryEmployee1.setFullName("Rajim");
        System.out.println(salaryEmployee1.toString());
        System.out.println(salaryEmployeeList);

//
//        List<String> nameList = salaryEmployeeList.stream()
//                .filter(e-> e.getMonthlySalary() > 1000)
//                .map(EmployeeMain::convertTONameList)
//                .collect(Collectors.toList());

        List<String> nameWithId = new ArrayList<>();
        for(SalaryEmployee salaryEmployee: salaryEmployeeList) {
            nameWithId.add(salaryEmployee.getFullName()
                    +"-"+salaryEmployee.getEmployeeId());
        }



        salaryEmployeeList.forEach(EmployeeMain::justPrint);

//        System.out.println(salaryEmployee.toString());
        // // Employee + CommissionEmployee (grossSale commissionRate) // Earning
        // BasePlusCommissionEmployee  // grosssale*commission + base
        // hourly employee hour earingperhour -> earning()


    }

    private static void justPrint(SalaryEmployee salaryEmployee) {
        System.out.println(salaryEmployee.getFullName()+" - "+salaryEmployee.getEmployeeId());
    }

    private static String convertTONameList(SalaryEmployee salaryEmployee) {
        return salaryEmployee.getFullName()+" - "+salaryEmployee.getEmployeeId();
    }

    /**
     *
     * @param fullName []
     * @param employeeId []
     * @param salary []
     * @return
     */

    private static SalaryEmployee createNewEmployee(String fullName,
                                                    String employeeId,
                                                    double salary) {
        SalaryEmployee salaryEmployee = new SalaryEmployee();
        salaryEmployee.setFullName(fullName);
        salaryEmployee.setEmployeeId(employeeId);
        salaryEmployee.setMonthlySalary(salary);
        return salaryEmployee;
    }
}
