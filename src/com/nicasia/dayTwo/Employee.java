package com.nicasia.dayTwo;

/**
 * @author rajim 2021-08-25.
 * @project IntelliJ IDEA
 */
public class Employee {
    private String fullName;
    private String employeeId;

    public Employee() {

    }

    public Employee(String fullName,
                    String employeeId) {
        this.fullName = fullName;
        this.employeeId = employeeId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }


    @Override
    public String toString() {
        return "Name::"+this.fullName
                +" Employee Id ::"+this.employeeId;
    }
}
