package com.nicasia.dayTwo;

/**
 * @author rajim 2021-08-25.
 * @project IntelliJ IDEA
 */
public class SalaryEmployee extends Employee {
    private double monthlySalary;

    public SalaryEmployee() {
        super();
    }

    public SalaryEmployee(String fullName, String employeeId,
                          double monthlySalary) {
        super(fullName, employeeId);
        this.monthlySalary = monthlySalary;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }


    @Override
    public String toString() {
        return super.toString() + " Salary :: "
                + this.monthlySalary;
    }
}
