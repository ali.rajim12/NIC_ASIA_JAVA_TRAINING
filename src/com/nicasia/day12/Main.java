package com.nicasia.day12;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-10.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {
        List<String> items =
                Arrays.asList("apple", "apple", "banana",
                        "apple", "orange", "banana", "papaya");


        Map<String, Long> results = items.stream()
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.counting()
                ));

        results.entrySet().forEach(System.out::println);
    }
}
