package com.nicasia.dayFour;

/**
 * @author rajim 2021-08-29.
 * @project IntelliJ IDEA
 */
public interface Payable {
    double totalDues();

   double totalDues(int test);
}
