package com.nicasia.dayFour;

/**
 * @author rajim 2021-08-29.
 * @project IntelliJ IDEA
 */
public class Invoice implements Payable {
    private int totalItems;
    private double itemPerPrice;


    @Override
    public double totalDues() {
        return this.totalItems * this.itemPerPrice;
    }


    @Override
    public double totalDues(int test) {
        return this.totalItems * this.itemPerPrice;
    }


    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }
}
