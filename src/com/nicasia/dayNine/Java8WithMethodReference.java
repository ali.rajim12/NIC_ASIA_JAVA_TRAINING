package com.nicasia.dayNine;
import java.util.*;

/**
 * @author rajim 2021-09-06.
 * @project IntelliJ IDEA
 */
public class Java8WithMethodReference {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("A", "B", "C");

        // method with lamda expression
        list.forEach(x-> SimplePrinter.print(x));

        // print list with method reference

        list.forEach(SimplePrinter::print);
    }
}
