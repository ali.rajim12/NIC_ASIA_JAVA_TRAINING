package com.nicasia.dayNine;

import com.nicasia.dayFive.Emp;

/**
 * @author rajim 2021-09-06.
 * @project IntelliJ IDEA
 */
public class Employee {

    private Integer id;
    private String name;

    public Employee(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
