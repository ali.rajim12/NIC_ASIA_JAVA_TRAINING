package com.nicasia.dayNine;

import java.util.function.Function;

/**
 * @author rajim 2021-09-06.
 * @project IntelliJ IDEA
 */
public class Java8ChainFunction {
    public static void main(String[] args) {
        Function<String, Integer> function1 = String::length;

        Function<Integer, Integer> function2 = x-> x*2;

        Integer result = function1.andThen(function2).apply("Rajim");

        System.out.println(result);
    }
}
