package com.nicasia.dayNine;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-06.
 * @project IntelliJ IDEA
 */
public class Java8Function3 {
    public static void main(String[] args) {
        List<Employee> list = Arrays.asList(
                new Employee(1, "mkyong"),
                new Employee(2, "zilap"),
                new Employee(3, "ali"),
                new Employee(4, "unknown"));

        Map<Integer, Employee> map = new HashMap<>();
        for( Employee employee : list) {
            map.put(employee.getId(), employee);
        }
        System.out.println(map);


        // input id return name

         list.stream()
                .map(e-> e.getId())
                .collect(Collectors.toList());


        Java8Function3 java8Function3 = new Java8Function3();


        Map<Integer, String> employeeMap = java8Function3
                .convertListToMap(list.stream()
                                .map(Employee::getId)
                                .collect(Collectors.toList()),
                        x -> java8Function3.getName(x, list));

        System.out.println(employeeMap);


    }

    private String getName(Integer x, List<Employee> list) {
        Optional<Employee> first = list.stream()
                .filter(e -> e.getId().equals(x))
                .findFirst();
        if(first.isPresent()) {
            return first.get().getName();
        }
        return null;
    }

    public <T, R> Map<T, R> convertListToMap(List<T> list,
                                             Function<T, R> func) {
        Map<T, R> result = new HashMap<>();
        for (T t : list) {
            result.put(t, func.apply(t));
        }
        return result;
    }

    public String getName(Employee employee) {
        return employee.getName();
    }

}
