package com.nicasia;
import com.nicasia.dayOne.Student;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here

        List<Student> studentList = new ArrayList<>();

//        Student[] students = new Student[4];


//        Arrays.asList(students);

        // Creating student list
        /**
         * Creating student list
         */
        // Using constructors
        studentList.add( new Student("Test 1", "1"));
        studentList.add( new Student("Test 2", "2"));
        studentList.add( new Student("Test 3", "3"));

        // Advance for loop

//        studentList.forEach(e-> System.out.println(e.getName()));

        for(Student student: studentList) {
            System.out.println(student.getName());
        }

//        // Using setter and getters
//        Student student = new Student();
//        student.setName("Test 4");
//        student.setRollNumber("4");
//        studentList.add(student);
//
//        Student student5 = createStudent("Test 5", "5");

//        Student student = new Student("Test", "1");
        // create one student
//        student.setName("Test");
//        student.setRollNumber("1");

    }

    /**
     *
     * @param name [full name ]
     * @param rollNumber [Int]
     * @return
     */

//    private static Student createStudent(String name, String rollNumber) {
//        return null;
//    }
    /**
     * Employee class
     * SalaryEmployee class
     */
}
