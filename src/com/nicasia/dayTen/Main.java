package com.nicasia.dayTen;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
/**
 * @author rajim 2021-09-07.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "A");
        map.put(2, "B");

        Set<Integer> integers = map.keySet();
//        map.values()


        for(Map.Entry<Integer, String> map1: map.entrySet()) {
            System.out.println("Key::"+map1.getKey()+" Value::"+map1.getValue());
        }

        System.out.println(map.get(1));

        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");

        for(String a: list) {
            System.out.println(a);
        }

        try {
            URL url = new URL("http://example.com");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
        } catch (Exception e) {
//            throw new Exception(e.getMessage());

        }

    }
}
