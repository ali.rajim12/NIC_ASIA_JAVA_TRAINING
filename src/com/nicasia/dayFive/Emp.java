package com.nicasia.dayFive;

/**
 * @author rajim 2021-09-01.
 * @project IntelliJ IDEA
 */
public class Emp {
    private int id;
    private String name;
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Name: "+this.getName() +" Age::"+getAge();
    }
}
