package com.nicasia.dayFive.employee;

import com.nicasia.dayFive.jdbc.JDBCConnector;
import com.nicasia.dayFive.jdbc.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class SalaryEmployee extends Employee {

    private double monthlySalary;

    @Override
    public double totalEarning() {
        return getMonthlySalary();
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    /**
     * Code to create salary employee
     */
    public void createSalaryEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Full Name");
        String fullName = scanner.nextLine();
        System.out.println("Enter Employee Id.");
        String employeeId = scanner.nextLine();
        System.out.println("Enter monthly Salary");
        double monthlySalary = scanner.nextDouble();
        // INSERT INTO salary employee
        try {
            Connection connection = JDBCConnector.getConnection();
            // Block to insert into emp table
            String query = Query.INSERT_SALARY_EMPLOYEE;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, fullName);
            preparedStmt.setDouble(2, monthlySalary);
            preparedStmt.setString(3, employeeId);
            System.out.println(preparedStmt.toString());
            preparedStmt.execute();
            JDBCConnector.closeConnection(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
