package com.nicasia.dayFive.employee;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public abstract class Employee {
    private String fullName;
    private String employeeId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public abstract double totalEarning();
}
