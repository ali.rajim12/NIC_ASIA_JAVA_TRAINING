package com.nicasia.dayFive.employee;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class CommissionEmployee extends Employee {
    private double grossSale;
    private double rate;

    @Override
    public double totalEarning() {
        return getGrossSale()*getRate();
    }

    public double getGrossSale() {
        return grossSale;
    }

    public void setGrossSale(double grossSale) {
        this.grossSale = grossSale;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
