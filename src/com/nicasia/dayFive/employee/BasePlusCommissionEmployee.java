package com.nicasia.dayFive.employee;

import java.util.Scanner;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class BasePlusCommissionEmployee extends CommissionEmployee {

    private double baseAmount;

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    @Override
    public double totalEarning() {
        return super.totalEarning() + getBaseAmount();
    }

    public void createBasePlusCommissionEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Full Name");
        String fullName = scanner.nextLine();
        System.out.println("Enter Employee Id.");
        String employeeId = scanner.nextLine();
        System.out.println("Enter Base Amount");
        double baseAmount = scanner.nextDouble();
        System.out.println("Enter Gross Sale");
        double grossSale = scanner.nextDouble();
        System.out.println("Enter rate");
        double rate = scanner.nextDouble();
        // insert into employee table


    }
}
