package com.nicasia.dayFive.employee;

/**
 * @author rajim 2021-08-26.
 * @project IntelliJ IDEA
 */
public class EmployeeDetail {
    private String fullName;
    private double totalEarning;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(double totalEarning) {
        this.totalEarning = totalEarning;
    }

    @Override
    public String toString() {
        return "Full name:"+this.getFullName()
                +" Earning: "+this.getTotalEarning();
    }
}
