package com.nicasia.dayFive;

import com.nicasia.dayFive.employee.BasePlusCommissionEmployee;
import com.nicasia.dayFive.employee.SalaryEmployee;
import com.nicasia.dayFive.jdbc.JDBCConnector;
import com.nicasia.dayFive.jdbc.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-01.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String choice = null;
        do {
            System.out.println("1. For Creating Salary Employee.");
            System.out.println("2. Commission Employee.");
            System.out.println("3. Base Plus Commission Employee.");
            System.out.println("4. Show all employee list.");
            System.out.println("Q for exit");
            choice = scanner.next();
            switch(choice) {
                case "1":
                    SalaryEmployee salaryEmployee = new SalaryEmployee();
                    salaryEmployee.createSalaryEmployee();
                    break;
                case "2":
                    System.out.println("2 clicked");
                    break;
                case "3":
                    BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee();
                    basePlusCommissionEmployee.createBasePlusCommissionEmployee();
                    break;
                case "4":
                    System.out.println("4 clicked");
                    break;
                case "q":
                    break;
                default:
                    System.out.println("Please enter valid input.");
                    break;
            }
        } while (!choice.equals("q"));
        System.out.println("Thank you for using this feature.");
//        int choice = scanner.nextInt();
//        while(choice)
//
//        switch(choice) {
//            case:
//        }
        // to print list saved in db
//        try {
//            Connection connection = JDBCConnector.getConnection();
//            Statement stmt = connection.createStatement();
//            ResultSet rs = stmt.executeQuery("select * from emp");
//            List<Emp> empList = new ArrayList<>();
//            while (rs.next()) {
//                Emp emp = new Emp();
//                emp.setId(rs.getInt("id"));
//                emp.setName(rs.getString("name"));
//                emp.setAge(rs.getInt("age"));
//                empList.add(emp);
//            }
//            // to find first one
//
//            Optional<Emp> first = empList.stream()
//                    .filter(e -> e.getAge() > 29)
//                    .findFirst();
//
//            // to collect detail into list
//
//            List<Emp> collect = empList.stream()
//                    .filter(e -> e.getAge() > 15)
//                    .collect(Collectors.toList());
//
//            System.out.println(Arrays.toString(collect.toArray()));
//
//            first.ifPresent(System.out::println);
//
//            // Block to insert into emp table
//            String query = Query.INSERT_EMP;
//            PreparedStatement preparedStmt = connection.prepareStatement(query);
//            preparedStmt.setInt (1, 3);
//            preparedStmt.setString (2, "Bishesh");
//            preparedStmt.setInt   (3, 35);
//            preparedStmt.execute();
//
//            JDBCConnector.closeConnection(connection);
//
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }
}
