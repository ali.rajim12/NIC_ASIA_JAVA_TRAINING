package com.nicasia.dayFive.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author rajim 2021-09-01.
 * @project IntelliJ IDEA
 */
public class JDBCConnector {
    /**
     * Method to get connection to jdbc
     *
     * @return
     */

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName(Configuration.DB_CLASSNAME);
            con = DriverManager.getConnection(
                    Configuration.DB_URL,
                    Configuration.DB_USER_NAME,
                    Configuration.DB_PASSWORD);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return con;
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
