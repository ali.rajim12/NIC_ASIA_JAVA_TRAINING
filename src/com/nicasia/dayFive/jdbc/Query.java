package com.nicasia.dayFive.jdbc;

/**
 * @author rajim 2021-09-01.
 * @project IntelliJ IDEA
 */
public interface Query {

    public static String INSERT_EMP = "insert into emp (id, name, age )"
            + " values (?, ?, ?)";

    public static String INSERT_SALARY_EMPLOYEE ="INSERT INTO `training`.`employee` \n" +
            "\t(\n" +
            "\t`full_name`, \n" +
            "\t`monthly_salary`, \n" +
            "\t`employee_id`\n" +
            "\t)\n" +
            "\tVALUES\n" +
            "\t(\n" +
            "\t?,\n" +
            "\t?\n" +
            "\t?\n" +
            "\t);";
}
