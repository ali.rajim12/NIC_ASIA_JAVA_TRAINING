package com.nicasia.dayOne;

/**
 * @author rajim 2021-08-24.
 * @project IntelliJ IDEA
 */

public class Student {
    private String name;
    private String rollNumber;

    public Student(String name, String rollNumber) {
        this.name = name;
        this.rollNumber = rollNumber;
    }

    public Student() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRollNumber() {
        return rollNumber;
    }


    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    @Override
    public String toString() {
        return "Name ::" + this.getName()
                + ", Roll Number::"
                + this.getRollNumber();
    }
}
