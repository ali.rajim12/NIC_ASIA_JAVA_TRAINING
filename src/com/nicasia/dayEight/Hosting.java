package com.nicasia.dayEight;

/**
 * @author rajim 2021-09-05.
 * @project IntelliJ IDEA
 */
public class Hosting {
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getWebsites() {
        return websites;
    }

    public void setWebsites(long websites) {
        this.websites = websites;
    }

    private int Id;
    private String name;
    private long websites;

    public Hosting(int id, String name, long websites) {
        Id = id;
        this.name = name;
        this.websites = websites;
    }

    @Override
    public String toString() {
        return "ID::"+this.Id +" Name:: "+this.name+" Websites::"+this.websites;
    }
}
