package com.nicasia.dayEight;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author rajim 2021-09-05.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {
        Stream<Hosting> hostingStream = Stream.of(
                new Hosting(1, "liquidweb.com", 80000),
                new Hosting(2, "linode.com", 90000));

        Map<Integer, String> hostingMap =
                hostingStream
                        .collect(Collectors.toMap(
                Hosting::getId, Hosting::getName
        ));

        for(Map.Entry<Integer, String> map: hostingMap.entrySet()) {
            System.out.println(map);
        }

        hostingMap.entrySet().forEach(System.out::println);
        // get individual entry
        String s = hostingMap.get(1);

        hostingMap.entrySet().forEach(System.out::println);

        // map with filter



//        hostingStream.map()
    }

    private static String createKey(Hosting e) {
        return e.getId() + " " + e.getName();
    }
}
